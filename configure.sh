cd ./lib/boost/
sh bootstrap.sh
./b2 toolset=emscripten link=static runtime-link=static --with-thread --with-system --with-filesystem --with-program_options --with-date_time
cd ../db/build_unix/
emconfigure ../dist/configure
emmake make
